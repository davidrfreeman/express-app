FROM node:16.16-alpine

ARG PORT=8080

WORKDIR /src/usr/app

COPY package.*json server.js index.html info.html ./

RUN npm install

RUN addgroup -g 1001 express && \
    adduser -S -u 1001 -h /express -s /sbin/nologin -g Express -G express express

USER 1001

EXPOSE 8080

ENTRYPOINT [ "npm", "start" ]
