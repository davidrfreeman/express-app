# CI/CD Example

Use the following instructions to follow along with the pipeline demo. In this demo, commands to run are done from a terminal and files are created and edited by using VS Code, a popular code editor.

## VS Code Install

VS Code can be installed by selecting the [MacOS install link](https://code.visualstudio.com/Download). Be aware that there is a version for Apple Silicon (M1) chipsets, and this may not be the correct install for you.

## Nodejs Install

It is recommended to install node version manager (NVM) to install nodejs and node package manager (NPM).

### NVM Install

Follow the instructions found in this [GitHub repo](https://github.com/nvm-sh/nvm#installing-and-updating) to install NVM. Once installed we will move on to installing Nodejs and NPM. Don't forget top follow the instructions on updating your shell configuration, typically .zshrc or .bashrc. The commands to install NVM will be run from your terminal. This app can be found in your applications folder, or by pressing the command button + spacebar to open the Spotlight Search. In the search bar, type `terminal` and then select the terminal app.

#### Installing nodejs using NVM

For our example we will use the latest long term support (LTS) version of node. This is v14 and is named fermium. `nvm` can list locally installed versions of node, as well as the available remote versions.

- locally installed versions: `nvm list`
- remote/available versions: `nvm list-remote`

To install v14 of node we can use the following command: `nvm install lts/fermium`

You should now confirm the installed versions of node and npm

- `node --version` or `node -v`
- `npm --version` or `npm -v`

## Container Runtime

We need to install a tool that allows us to build and run containers

### Rancher Desktop

Installing Rancher Desktop for Mac is very simple, and can be done following [these instructions](https://docs.rancherdesktop.io/getting-started/installation).

## Create a new node project

To create your first nodejs project we will use `npm init`. Follow the prompts to provide basic info to initialize your project and output a `package.json` file. Once this file has been created, run `npm install express --package-lock-only --no-package-lock`. This will add express as a dependency in your package.json file, without installing any node_modules just yet.

Your package.json should container at least the following

//package.json

```json
{
  "name": "docker_web_app",
  "version": "1.0.0",
  "description": "Node.js on Docker",
  "author": "First Last <first.last@example.com>",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "dependencies": {
    "express": "^4.16.1"
  }
}
```

## Create app files

Copy content of server.js and paste it locally into a file also named server.js

//server.js

```js
"use strict";

const express = require("express");

// Constants
const PORT = process.env.PORT || 3000;

// App
const app = express();

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.listen(PORT);
console.log(`Listening on port ${PORT}`);
```

### Test app locally

To test your app locally, open a terminal and navigate to the directory that contains your repository. Then run `npm install` from the terminal to install the required dependencies. Once the installation has completed you can run your app. From a terminal run `node server.js`. This will start the express webserver. Open a browser and open the following to test your app `http://localhost:3000/` or run a curl of `curl localhost:3000/`

You should see the output the text in line 12 of server.js

```js
res.send("Hello World");
```

## Create Dockerfile

Now lets test running this app within a docker container. We can create a dockerfile to be used to create a container image. To do this we can use VS Code to create a new file named `Dockerfile` with no file extension, and also a file named `.dockerignore` which will be used by the docker build.

//.dockerignore

```txt
.git
.gitignore
node_modules
npm-debug.log
Dockerfile*
docker-compose*
README.md
LICENSE
.vscode
```

//Dockerfile

```Dockerfile
FROM node:16.16-alpinexs

ENV PORT=8080

WORKDIR /src/usr/app

COPY package*.json .

RUN npm install

COPY . .

EXPOSE 8080

ENTRYPOINT [ "npm", "start" ]
```

### Test Dockerfile locally

To test your Dockerfile, you will want to create an image, which will then be used to run a container. First you will run a build, which will create the image, and then you will run a container

```bash
docker build --progress=plain -t localhost/app-server:1.0 .
docker run -it --rm --name app-server -p 3000:8080 localhost/app-server:1.0
curl localhost:3000/
```

You should see receive a 200 status code response, as well as the text in the response in server.js

```txt
➜ curl localhost:3000/
Hello World!%
```

## GitLab CI

### Account Creation

If you do not already have a GitLab account (gitlab.com) you can create one [here](https://gitlab.com/users/sign_up).

### GitLab Runner

To install GitLab Runner on your local machine, we will use a homebrew install.

`brew install gitlab-runner`

#### Register Runner

#### Start Runner

`brew services start gitlab-runner`

#### Configure Runner

You now need to edit your runner's config, located at `~/.gitlab-runner/config.toml`. I recommend using vim or vs code to edit this file. The following example opens the file within vs code.

`code ~/.gitlab-runner/config.toml`

The following fields need to be updated/added

- privileged = true
- volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
- pull_policy = "if-not-present"
- wait_for_services_timeout = 3

You can also up the `concurrent` value to allow for multiple jobs to run at once. This will speed up your pipeline run times, but you will need to keep an eye on the load this puts on your machine. If you notice a performance issue, you can edit your docker desktop's resources `docker desktop -> dashboard -> settings -> resources`

example config

```toml
concurrent = 4
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "SpaceCAMP MacBook"
  url = "https://gitlab.com"
  token = "<project or group runner token>"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "python:3.9"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
    pull_policy = "if-not-present"
    wait_for_services_timeout = 3
    shm_size = 0
```

### Create CI file

Add test shell script to your project named `message.sh`

```sh
#!/usr/bin/env sh

message="This is a message from a shell script"

echo $message
```

Now add a ci file that echoes a message to standard out (STDOUT) and runs a simple bash script. This file **has** to be named `.gitlab-ci.yml`

```yaml
image: alpine:latest

stages:
  - test-stage

test-job:
  stage: test-stage
  variables:
    MSG: "Hello World"
  script:
    - echo $MSG
    - sh message.sh
```

### Test CI file

Commit and push the ci file to your project. This push action should start a pipeline, which you can view by visiting the GitLab UI, navigating to your project, and selecting CI/CD on the left-hand side.

From this page, you should see a pipeline begin. Select it, and then select the running job. From here you will see the log output from the job. This output should be the value of the `MSG` variable you created.

#### Node app build

Now you are ready to test building your node app via a pipeline.

Update you ci file to the following:

```yaml
image: docker:latest
stages:
  - build

build:
  services:
    - docker:dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
```

Go ahead and push this updated file, and watch your new pipeline run this job. After it completes, it will push an image to the GitLab container registry, which you can pull down to test that this image functions the same as the locally built image we made earlier.

### Adding stages and jobs

Now we can test updating our app to serve html files, as well as updating our ci file to add test and deploy stages.

Add/Update the following files to test this change

#### Add index and info HTML files

```html
<!-- index.html -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Express App Test</title>
  </head>
  <body>
    <h1>Hello World</h1>
    <p>This is an html paragraph</p>
    <a href="/info">Info</a>
  </body>
</html>
```

```html
<!-- info.html -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Info</title>
  </head>
  <body>
    <h2>INFO</h2>
    <p>Info page</p>
    <a href="/">HOME</a>
  </body>
</html>
```

#### Update server.js

```js
"use strict";

const express = require("express");
const path = require("path");

// Constants
const PORT = process.env.PORT || 3000;

// App
const app = express();

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "/index.html"));
});

app.get("/info", (req, res) => {
  res.sendFile(path.join(__dirname, "info.html"));
});

app.listen(PORT);
console.log(`Listening on port ${PORT}`);
```

#### Update Dockerfile

```Dockerfile
FROM node:16.16-alpine

ARG PORT=8080

WORKDIR /src/usr/app

COPY package.json server.js index.html info.html ./

RUN npm install

EXPOSE 8080

ENTRYPOINT [ "node", "server.js" ]
```

#### Update .gitlab-ci.yml

```yaml
image: docker:latest
stages:
  - build
  - test
  - deploy

build:
  services:
    - docker:dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  stage: build
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG

test:
  stage: test
  script:
    - echo "This is a testing stage"

dockerhub-publish:
  when: manual
  stage: deploy
  needs:
    - build
  variables:
    GITLAB_IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    DOCKER_HUB_IMAGE_TAG: $DOCKERHUB_USERNAME/express-app:0.1
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $GITLAB_IMAGE_TAG
    - docker tag $GITLAB_IMAGE_TAG $DOCKER_HUB_IMAGE_TAG
    - docker login -u $DOCKERHUB_USERNAME -p $DOCKERHUB_TOKEN
    - docker push $DOCKER_HUB_IMAGE_TAG

include:
  - template: Security/SAST.gitlab-ci.yml
```

Push all of these updates to your project, and view the new pipeline. This time you will see three stages, instead of one. The second stage (test) only echoes something at the moment, and the third will only run if you manually select the run button. This job is an example of promoting an image to a production environment. Specifically, it pushes the image to a public repository in Dockerhub, making the image available for anyone else to download.
